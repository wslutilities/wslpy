from wslpy.core.check import is_interop_enabled, is_wsl

__all__ = [
    "is_interop_enabled",
    "is_wsl",
]
